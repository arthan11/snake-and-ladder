
Element = function(parent, x, y, id) {
  this.parent = parent;
  this.object = null;

  this.create_element = function(x, y, id) {
    this.object = $('<div id="pole_'+id+'" class="pole"></div>')
    _left = 50 + 10 + x*107;
    _top = 50 + 58 + y*111;
    this.object.css('left', _left);
    this.object.css('top', _top);
    this.object.appendTo(this.parent.object);
  };

  this.create_element(x, y, id);
};

Board = function(parent) {
  this.parent = $(parent);
  this.object = null;
  this.elements = [];
  //this.sequence = [];

  this.elements_map = [
  ];

  for (var y=0; y < 7; y++) {
    for (var x=0; x < 10; x++) {
      if (y % 2 == 0) {
        this.elements_map.push( 70 - ((y+1)*10) + (1+x) );
      } else {
        this.elements_map.push( 70 - ((y+1)*10) + (10-x) );
      };
    };
  };


  this.clear_elements = function() {
    this.elements = [];
  };

  this.create_elements = function(cols, rows) {
    for (var j = 0; j < rows; j++) {
      for (var i = 0; i < cols; i++) {
        var el = new Element(this, i, j, (this.elements_map[j*10+i]) );
        this.elements.push(el);
      };
    };
  };

  this.add_img = function(id, img_name, width, height, left, top, border) {
    img = $('<div id="'+id+'"></div>');
    img.css('background-image', 'url("static/img/'+img_name+'")');
    if (border) {
      img.addClass('rounded-border');
    };
    img.css('background-size', 'cover');
    img.css('position', 'absolute');
    img.width(width);
    img.height(height);
    img.css('left', left);
    img.css('top', top);
    img.appendTo(this.object);
  };

  this.create_board = function() {
	this.object = $('<div class="board"></>');
	this.object.appendTo(this.parent);

    this.add_img('plansza', 'plansza.jpg', 1119, 869, 50, 50, true);
    this.create_elements(10, 7);

    this.add_img('pionek1', 'pionek1.png', 64, 64, 0, 800, false);
    this.add_img('pionek2', 'pionek2.png', 64, 64, 170, 800, false);

  };

  this.move_to = function(div, pole_nr) {
    pole = $('#pole_'+pole_nr);
    x = pole.position().left;
    y = pole.position().top + 20;
    div.css('left', x);
    div.css('top', y);
    //this.sequence.push({e: div, p: "transition.bounceOut"})
    //$.Velocity.RunSequence(this.sequence);
  };
  
  this.create_board();
};



$(function() {

  board = new Board('body');
  dice_roller = new DiceRoller('body', x=450, y=25);


  $( "#btnRoll1" ).click(function() {
    roll = dice_roller.roll(800, dices=1, mod=0);
    board.move_to($('#pionek1'), roll);
  });

});
